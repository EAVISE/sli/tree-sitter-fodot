# tree-sitter-fodot

A [tree-sitter](https://tree-sitter.github.io/tree-sitter/) parser for [FO(.)](https://en.wikipedia.org/wiki/FO(.)).
Currently, the parser supports the most "standard" FO(.), including the concepts you would expect to find in the vocabulary, structure and theory.
There is no support (yet) for advanced concepts, such as constructors, intensional reasoning, ...
Many of the syntactic sugars have also not been implemented yet.




## Usage

This parser has interfaces for many programming languages, such as Python, Rust, C++, and more.
See the [getting started section](https://tree-sitter.github.io/tree-sitter/using-parsers) in the tree-sitter docs.


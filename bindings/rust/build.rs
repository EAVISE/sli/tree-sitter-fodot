use bindgen::callbacks::ParseCallbacks;

#[derive(Debug)]
pub struct BindgenCallback {
    name: &'static str,
    first: bool,
}

impl BindgenCallback {
    pub fn new(name: &'static str) -> Self {
        BindgenCallback {
            name,
            first: false,
        }
    }
}

impl ParseCallbacks for BindgenCallback {
    fn item_name(&self, _original_item_name: &str) -> Option<String> {
        dbg!(_original_item_name);
        if !self.first {
            // "FodotSymbols".to_string().into()
            self.name.to_string().into()
        } else {
            panic!("Only expected one item!!");
        }
    }

    fn enum_variant_name(
            &self,
            _enum_name: Option<&str>,
            original_variant_name: &str,
            _variant_value: bindgen::callbacks::EnumVariantValue,
        ) -> Option<String> {
        let mut rustified_name = String::new();
        let mut was_under_score = true;
        for char in original_variant_name.chars() {
            if char.eq(&'_') {
                was_under_score = true;
            } else if was_under_score {
                rustified_name.push(char.to_ascii_uppercase());
                was_under_score = false;
            } else {
                rustified_name.push(char);
            }
        }
        rustified_name.into()
    }
}
fn main() {
    let src_dir = std::path::Path::new("src");

    let mut c_config = cc::Build::new();
    c_config.include(&src_dir);
    c_config
        .flag_if_supported("-Wno-unused-parameter")
        .flag_if_supported("-Wno-unused-but-set-variable")
        .flag_if_supported("-Wno-trigraphs");
    #[cfg(target_env = "msvc")]
    c_config.flag("-utf-8");

    let parser_path = src_dir.join("parser.c");
    c_config.file(&parser_path);

    // If your language uses an external scanner written in C,
    // then include this block of code:

    /*
    let scanner_path = src_dir.join("scanner.c");
    c_config.file(&scanner_path);
    println!("cargo:rerun-if-changed={}", scanner_path.to_str().unwrap());
    */

    c_config.compile("parser");
    println!("cargo:rerun-if-changed={}", parser_path.to_str().unwrap());

    let bindings = bindgen::Builder::default()
        .header("src/parser.c")
        .clang_arg("-Isrc/")
        .parse_callbacks(Box::new(BindgenCallback::new("FodotSymbols")))
        .newtype_enum(".*")
        .allowlist_file(r"parser\.c")
        .allowlist_item("ts_symbol_identifiers")
        .blocklist_file(r".*\.h.*")
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = std::path::PathBuf::from(std::env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    // If your language uses an external scanner written in C++,
    // then include this block of code:

    /*
    let mut cpp_config = cc::Build::new();
    cpp_config.cpp(true);
    cpp_config.include(&src_dir);
    cpp_config
        .flag_if_supported("-Wno-unused-parameter")
        .flag_if_supported("-Wno-unused-but-set-variable");
    let scanner_path = src_dir.join("scanner.cc");
    cpp_config.file(&scanner_path);
    cpp_config.compile("scanner");
    println!("cargo:rerun-if-changed={}", scanner_path.to_str().unwrap());
    */
}
